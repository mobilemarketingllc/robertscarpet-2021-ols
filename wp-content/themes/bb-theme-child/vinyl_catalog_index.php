<?php
/*
Template Name: Vinyl Catalog Index 
*/

get_header(); 
$table_posts = $wpdb->prefix.'posts';
$table_meta = $wpdb->prefix.'postmeta';	
?>
<style>
.brand_listing {
    padding: 20px;
}
.brand_listing > ul {
    margin-bottom: 0px;
    padding-left: 0px;
    text-align: center;
}
.brand_listing ul li{
	display:inline;
  padding: 0px 15px;
}
.brand_listing ul li a {
    line-height: 1.5em!important;
}
.brand_listing ul li:not(:first-child) {
    border-left: 2px solid #000!important;
}
.brand_wrapper {
    column-count: 4;
    column-gap: 0em;
    max-width: 1100px;
    margin: 0 auto;
}
.brand_collection_listing {
    break-inside: avoid;
    padding: 10px;
}
.brand_wrapper .brand_collection_listing h2 {
    font-size: 20px!important;
    margin: 0px!important;
	font-weight: 700!important;
}
.brand_collection_listing ul {
    padding-left: 20px;
}
@media(max-width: 992px) {
    .brand_wrapper {
      column-count: 3;
  }
}

@media(max-width: 600px) {
    .brand_wrapper {
      column-count: 2;
  }
  .brand_listing ul li {
	  width: 50%!important;
	  display: inline-block!important;
	  float: left!important;
	  margin: 0px;
	  text-align: left;
	}	
	.brand_listing ul li:not(:first-child) {
		border-left: none!important;
	}	
}

@media(max-width: 400px) {
    .brand_wrapper {
      column-count: 1;
  }
}
</style>
<div class="fl-content-full container">
   <div class="row">
       <div class="fl-content col-md-12">
           <?php
          
                $brand_sql = "SELECT  DISTINCT($table_meta.meta_value) as brand_facet
                            FROM $table_meta
                            INNER JOIN $table_posts ON $table_posts.ID = $table_meta.post_id 
                            WHERE post_type = 'luxury_vinyl_tile' AND meta_key = 'brand_facet' ORDER BY brand_facet ASC";

                $data_brand =$data_brand_list = $wpdb->get_results($brand_sql);   

                if(is_array($data_brand) && count($data_brand) > 0){
                    ?>
                    <div class="brand_listing">
                        <ul>
                            <?php        
                            foreach($data_brand_list as $brand_list){
                                $brand_slug = str_replace(" ","_",$brand_list->brand_facet);
                                ?>
                                <li><a href="#<?php echo $brand_slug;?>"><strong><?php echo $brand_list->brand_facet;?></strong></a></li>
                                <?php
                            }
                            ?>
                        </ul>    
                    </div>
					<div class="clearfix"></div>
                    <div class="brand_wrapper">
                    <?php
                        foreach($data_brand as $brand){
                            $brand_slug = str_replace(" ","_",$brand->brand_facet);
                        ?>
                            <div class="brand_collection_listing" id="<?php echo $brand_slug;?>">
                                <h2><?php echo $brand->brand_facet;?></h2>
                                <ul>
                                    <?php
                                    $args = array(
                                        "post_type" => "luxury_vinyl_tile",
                                        "post_status" => "publish",
                                        "orderby" => "title",
                                        "order" => "ASC",
                                        "posts_per_page" => -1,
                                        'meta_query' => array(
                                            array(
                                            'key' => 'brand_facet',
                                            'value' => $brand->brand_facet,
                                            'compare' => '='
                                            )
                                        ),
                                        'meta_key' => 'collection'
                                    );
                                    add_filter('posts_groupby', 'query_group_by_filter'); 
                                    $data_collection = new WP_Query( $args );

                                    while($data_collection->have_posts() ): $data_collection->the_post(); 
                                        //collection field
                                        $meta_values = get_post_meta( get_the_ID(),'collection');  
                                        $collection = $meta_values[0];
                                        
                                        echo "<li><a href='".get_the_permalink()."'>".$collection."</a></li>";
                                    endwhile; wp_reset_postdata();
                                ?>
                                </ul>
                            </div>    
                        <?php    
                        }
                    ?>
                    </div>
                    <?php 
                }
                
           ?>
       </div>
   </div>
</div>

<?php get_footer(); ?>