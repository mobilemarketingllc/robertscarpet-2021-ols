<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("flooring finder",get_stylesheet_directory_uri()."/script.js","","",1);
  //  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

//get ipaddress of visitor

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
    
    global $wpdb;



  //  echo 'no cookie';
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];

        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
        if(isset($_COOKIE['robert_store'])){

            $store_location = $_COOKIE['robert_store'];
        }else{

            $store_location = '';
           
          }

        //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

        $storeposts = $wpdb->get_results($sql);

        $storeposts_array = json_decode(json_encode($storeposts), true);      

       
        if($store_location ==''){
           // //write_log('empty loc');           
            $store_location = $storeposts_array['0']['ID'];
            $store_distance = round($storeposts_array['0']['distance'],1);
        }
        else{

            ////write_log('noempty loc');
            $key = array_search($store_location, array_column($storeposts_array, 'ID'));
            $store_distance = round($storeposts_array[$key]['distance'],1);           

        }
        $content ='';
        $content .= '<div class="header_store"><div class="store_wrapper">';
        $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><div class="title-prefix">'.get_the_title($store_location).'  - <b>'.$store_distance.' MI</b><i class="fa fa-caret-down" aria-hidden="true"></i></div></a>';
        $content .= '<div class="store-add">'.get_field('address', $store_location).' '.get_field('city',$store_location).', '.get_field('state',$store_location).' '.get_field('postal_code', $store_location).'</div>';
        //$content .= '<p class="store-hour">'.get_field('hours', $store_location).' | '.get_field('phone', $store_location).'</p>';
        
        if(get_field('closed', $store_location)){
            $content .= '<p class="store-hour redtext">'.get_field('hours', $store_location).'</p>';
            $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        }else{
            $content .= '<p class="store-hour">'.get_field('hours', $store_location).'</p>';
        }
        $content .= '<p class="store-hour"><a href="tel:'.get_field('phone', $store_location).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $store_location).'</a></p>';
       // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">Change Location</a>';
        $content .= '<div class="title-prefix" style="font-style: italic;font-size:12px;"><a href="https://www.robertscarpet.com/about/flooring-locations/">View All Locations</a></div>';
        $content .= '</div></div>';
		


        echo $content;
       wp_die();
}

add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );



function choose_location_listing(){

    global $wpdb;
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );

        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000 ORDER BY distance LIMIT 0, 11";

        $storeposts = $wpdb->get_results($sql);

        $storeposts_array = json_decode(json_encode($storeposts), true);  

    $content = '<div id="storeLocation" class="changeLocation">';
    $content .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
    $content .= '<div class="content">'; 
  
    foreach ( $storeposts as $post ) {
     
        $content .= '<div class="store_wrapper" id ="'.$post->ID.'">';
        $content .= '<div class="title-prefix">'.get_the_title($post->ID).'  - <b>'.round($post->distance,1).' MI</b></div>';
        $content .= '<div class="store-add">'.get_field('address', $post->ID).' '.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</div>';
        
        if(get_field('closed', $post->ID)){
            $content .= '<p class="store-hour redtext">'.get_field('hours', $post->ID).'</p>';
            $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        }else{
            $content .= '<p class="store-hour">'.get_field('hours', $post->ID).'</p>';
        }
        $content .= '<p class="store-hour"><a href="tel:'.get_field('phone', $post->ID).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</a></p>';
        $content .= '<a href="javascript:void(0)" data-id="'.$post->ID.'" data-distance="'.round($post->distance,1).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
        $content .= '</div>'; 
    }
    
    $content .= '</div>';
    $content .= '</div>';

    echo $content;
    wp_die();
}

//add_shortcode('chooselisting', 'choose_location_listing');
add_action( 'wp_ajax_nopriv_choose_location_listing', 'choose_location_listing' );
add_action( 'wp_ajax_choose_location_listing', 'choose_location_listing' );

//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {

        $content = '<div class="store_wrapper">';
        $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><div class="title-prefix">'.get_the_title($_POST['store_id']).'  - <b>'.$_POST['distance'].' MI</b><i class="fa fa-caret-down" aria-hidden="true"></i></div></a>';
        $content .= '<div class="store-add">'.get_field('address', $_POST['store_id']).' '.get_field('city',$_POST['store_id']).', '.get_field('state',$_POST['store_id']).' '.get_field('postal_code', $_POST['store_id']).'</div>';
        

        if(get_field('closed', $_POST['store_id'])){
            $content .= '<p class="store-hour redtext">'.get_field('hours',$_POST['store_id']).'</p>';
            $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
            $content .= '<p class="store-hour"><a href="tel:'.get_field('phone', $_POST['store_id']).'"><i class="fa fa-phone-alt"></i> '.get_field('phone', $_POST['store_id']).' </a></p>';
        }else{
            $content .= '<p class="store-hour">'.get_field('hours',$_POST['store_id']).'</p>';
			$content .= '<p class="store-hour"> <a href="tel:'.get_field('phone', $_POST['store_id']).'"><i class="fa fa-phone-alt"></i>'.get_field('phone', $_POST['store_id']).'</a></p>';
        }

       // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link">Change Location</a>';
       $content .= '<div class="title-prefix" style="font-style: italic;font-size:12px;"><a href="https://www.robertscarpet.com/about/flooring-locations/">View All Locations</a></div>';
        $content .= '</div>';

        echo $content;

    wp_die();
}

//check out flooring FUnctionality

add_action( 'wp_ajax_nopriv_check_out_flooring', 'check_out_flooring' );
add_action( 'wp_ajax_check_out_flooring', 'check_out_flooring' );

function check_out_flooring() {

    $arg = array();
    if($_POST['product']=='carpeting'){

        $posttype = array('carpeting');

    }elseif($_POST['product']=='hardwood_catalog,laminate_catalog,luxury_vinyl_tile'){

        $posttype = array('hardwood_catalog','laminate_catalog','luxury_vinyl_tile');

    }elseif($_POST['product']=='tile_catalog' || $_POST['product']=='tile'){

        // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
        $posttype = array('tile_catalog');
    }

    $parameters =  explode(",",$_POST['imp_thing']);

foreach($parameters as $para){
     if($para == 'hypoallergenic'){

        $arg['hypoallergenic'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50
        );
     }elseif($para == 'diy_friendly'){

        $arg['diy_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'installation_facet',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'installation_method',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
            ),             
        );
     }elseif($para == 'aesthetic_beauty'){

        $arg['aesthetic_beauty'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'brand',
                    'value'   => 'Anderson Tuftex',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'brand',
                    'value'   => 'Karastan',
                    'compare' => '=',
                    
                ),
            ),
        );
     }elseif($para == 'eco_friendly'){
        $arg['eco_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Eco',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );
     }
     elseif($para == 'pet_friendly'){

        $arg['pet_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Pet',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );

        
     }elseif($para == 'easy_to_clean'){
        
        $arg['easy_to_clean']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'durability'){
        
        $arg['durability']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }
     elseif($para == ''){
        
        $arg['colors']=array(
            'post_type'      => array( 'luxury_vinyl_tile', 'laminate_catalog','hardwood_catalog','carpeting','tile' ),
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
        );

        
     }
     elseif($para == 'stain_resistant'){
        
        $arg['stain_resistant'] =array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Stain Resistant',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'Collection',
                    'value'   => 'Bellera',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'budget_friendly'){
        
        $arg['budget_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'fiber',
                    'value'   => array( 'SmartStrand','Stainmaster','Bellera','Pet R2x' ),
                    'compare' => 'IN',
                    
                ),
            ),
        );

        
     }elseif($para == 'color'){
        
        $arg['color'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50            
        );

        
     }

}
 
     $i = 1;

     $find_posts = array();
     foreach($parameters as $para){
        $wp_query[$i] = new WP_Query($arg[$para]);
        
        
        $find_posts = array_merge( $find_posts , $wp_query[$i]->posts);        

        $i++;
     }
     
     //print_r($find_posts);
     if (!$find_posts ) {
       
            $args =array(
                'post_type'      => $posttype,
                'post_status'    => 'publish',
                'posts_per_page' => 50,
                'orderby'        => 'rand'
            );
            $find_posts   = new WP_Query($args);
           // var_dump($args , $find_posts);
            $find_posts = $find_posts->posts;
     }

     if ( $find_posts ) {
          
        $content ="";

        foreach ( $find_posts as $post ) {

            $image = swatch_image_product_thumbnail($post->ID,'222','222');
            $sku = get_field( "sku", $post->ID );
           
     $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="'.$image.'" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>'.get_field( "collection", $post->ID ).'</h4> 
                                <h3>'.get_field( "color", $post->ID ).'</h3>
                                <p class="brand"><small><i>'.get_field( "brand", $post->ID ).'</i></small></p>
                                <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
                                Order sample
                                </a> -->
                                <a class="prodLink" href="'.get_permalink($post->ID).'">See info</a>
                            </div>
                            
                        </div>
                    </div>';
        }

        $content .='<div class="text-center buttonWrap" data-search="'.$_POST['imp_thing'].'">
                    <a class="button" href="javascript:void(0)">view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div>';

    }

    echo $content;
    wp_die();
}

add_filter( 'facetwp_facet_orderby', function( $orderby, $facet ) {
    if ( 'brand' == $facet['name'] ) {
        
        global $wp_query;
        $post = $wp_query->post;
        $slug = get_post_field( 'post_name', @$post->post_parent);
        $product_cat = array('carpet'=> 'FIELD(f.facet_display_value, "Karastan", "Dixie Home", "Stanton","Masland", "Fabrica",  "Anderson Tuftex", "Shaw Floors", "Mohawk","Phenix","Dream Weaver","Nourison","Philadelphia Commercial","Nourtex","Hagaman")',
                            'hardwood' => 'FIELD(f.facet_display_value,"Greenworld Industries", "Robbins", "Cali Bamboo","Karastan", "Mohawk","Cali Hardwoods",  "Anderson Tuftex", "Shaw Floors","Armstrong","Bruce","Atelier","Duchateau Signature","The Guild","Emily Morrow" , "Mannington","US Floors","COREtec","Greenclaimed")',
                            'laminate' => 'FIELD(f.facet_display_value, "Mohawk", "Shaw Floors","Revwood","Revwood Plus","Revwood Select")',
                            'vinyl' => 'FIELD(f.facet_display_value,"Coretec", "Karastan","Republic", "Armstrong", "Greenclaimed", "Pergo","Atelier","Duchateau Signature","The Guild","Greenworld Industries", "Mannington","Floorte", "Shaw Floors","Philadelphia Commercial")',
                        'tile' => 'FIELD(f.facet_display_value, "Shaw Floors", "Daltile","Emser","American Olean")');
        $orderby = @$product_cat[$slug];
    }
    return $orderby;
}, 10, 2 );


add_filter( 'gform_field_value_quoteproductname', 'populate_quoteproductname' );
function populate_quoteproductname( $value ) {

    if(isset($_GET['product_id'])) {

        $quoteproductname = get_the_title($_GET['product_id']);

        return $quoteproductname;

    }   
} 

add_filter( 'gform_field_value_quoteproductsku', 'populate_quoteproductsku' );
function populate_quoteproductsku( $value ) {

    if(isset($_GET['product_id'])) {

        $quoteproductsku = get_post_meta( $_GET['product_id'], 'sku', true );

        return $quoteproductsku;

    }   
}

add_filter( 'gform_field_value_quoteproducturl', 'populate_quoteproducturl' );
function populate_quoteproducturl( $value ) {

    if(isset($_GET['product_id'])) {

        $quoteproducturl = get_permalink($_GET['product_id']);

        return $quoteproducturl;

    }   
}
add_filter( 'facetwp_indexer_row_data', function( $rows, $params ) {
    if ( 'fiber' == $params['facet']['name'] ) {
        $post_id = (int) $params['defaults']['post_id'];
        $field_value = get_post_meta( $post_id, 'fiber', true );
        $new_row = $params['defaults'];
        $args = array("Smaster Live Well Other" , "Smpetprtct" , "Stainmaster" , "Stainmaster Deluxe", 
                    "Stainmaster Extra Life Tactess" , "Stainmaster Premier","Stainmaster PetProtect", 
                    "100% STAINMASTER® Luxerell™ BCF nylon" , "Stainmaster Masterlife, Ultralife" ,
                    "100% STAINMASTER® Tactesse® BCF nylon" , "Stainmaster Masterlife, Ultralife" ,
                    "100% STAINMASTER® TruSoft™ BCF nylon Type 6,6" , "Stainmaster Masterlife, Ultralife",
                    "100% STAINMASTER® BCF nylon" , "100% STAINMASTER® BCF nylon Type 6,6" ,
                    "100% STAINMASTER® Luxerell™ BCF nylon Type 6,6","100% STAINMASTER® SuperiaSD™ BCF nylon type 6,6",
                    "100% STAINMASTER® Tactesse® BCF nylon 6, 6","100% STAINMASTER® Xtra Life Tactesse® BCF nylon",
                    "100% STAINMASTER® Xtra Life nylon","100% STAINMASTER® nylon","100% STAINMASTER® Solution Dyed BCF nylon 6,6",
                    "100% STAINMASTER® ExtraBody II™ BCF nylon","100% STAINMASTER Solution Dyed BCF Nylon 6,6"   
    );
        if(in_array(trim($field_value) , $args)){
            $new_row['facet_value'] = "Stainmaster";
            $new_row['facet_display_value'] = "Stainmaster"; 
         }else{
            $new_row['facet_value'] = $field_value; // value
            $new_row['facet_display_value'] = $field_value; // label
         }
        $rows[] = $new_row;
    }
    return $rows;
}, 10, 2 );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   // //write_log($sql_delete);	
}
add_filter( 'auto_update_plugin', '__return_false' );

function my_cache_lifetime( $seconds ) {
  return 5184000; // 10 day. Default: 3600 (one hour)
}
add_filter( 'facetwp_cache_lifetime', 'my_cache_lifetime' );



//theme loop template changes for instock PLP
add_filter( 'facetwp_template_html', function( $output, $class ) {
    
    $prod_list = $class->query;  
    ob_start();       
   
        $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new.php';
      
    require_once $dir;    
    return ob_get_clean();
}, 10, 2 );


//single instock post type template

add_filter( 'single_template', 'childtheme_get_custom_post_type_template' );

function childtheme_get_custom_post_type_template($single_template) {
    global $post;

    if ($post->post_type != 'post'){    

            $single_template = get_stylesheet_directory().'/product-listing-templates/single-'.$post->post_type.'.php';  
    
    }
        return $single_template;
}
